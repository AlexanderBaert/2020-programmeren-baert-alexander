﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class Hoofdstuk2B
    {
        static public void VarianbelenHoofdletters()
        {
            Console.Write("Wat is je naam? ");
            string naam = Console.ReadLine();
            Console.WriteLine("Dus je naam is" + naam.ToUpper());

        }
        static public void Optellen()
        {
            Console.WriteLine("Wat is het eerste getal?");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wat is het tweede getal?");
            int getal2 = Convert.ToInt32(Console.ReadLine());

            int som = getal1 + getal2;
            Console.WriteLine("De som is" + som);

        }
        static public void VerbruikWagen()
        {
            Console.WriteLine("geef het aantal liter in tank voor de rit");
            double AantalLiterInTank1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("geef het aantal lieter in tank na de rit");
            double AantalLiterInTank2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("geef kilometerstand1 van je auto voor de rit");
            double AantalKilometer1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("geef kilometerstand2 van je auto na de rit");
            double AantalKilometer2 = Convert.ToDouble(Console.ReadLine());

            double a = (100 * (AantalLiterInTank1 - AantalLiterInTank2) / (AantalKilometer2 - AantalKilometer1));
            Console.WriteLine("het verbruik van de auto is:" + a);
            Console.WriteLine("Het afgeronde verbruik van de auto is :"+ Math.Round(a, 2));
        }
        static public void BeetjeWiskunde()
        {
            int a = -1 + 4 * 6;
            int b = (35 + 5) % 7;
            float c = 14 + -4 * 6 / 11;
            float d = 2 + 15 / 6 * 1 - 7 % 2;
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
        }
        static public void Gemiddelde()
        {
            int a = 18;
            int b = 11;
            int c = 8;
            int d = 3;
            float f = (a + b + c) / d;
            Console.WriteLine(f);

        }
        static public void Maaltafels()
        {
            int a = 411;
            int b = 1;
            int c = 2;
            int d = 3;
            int e = 4;
            int f = 5;
            int g = 6;
            int h = 7;
            int i = 8;
            int j = 9;
            int k = 10;

            int result = b * a;

            Console.WriteLine(b+" * " + a + " is " +  result );
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result2 = c * a;
            Console.WriteLine(c + " * " + a + " is " + result2);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result3 = d * a;
            Console.WriteLine(d + " * " + a + " is " + result3);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result4 = e * a;
            Console.WriteLine(e + " * " + a + " is " + result4);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result5 = f * a;
            Console.WriteLine(f + " * " + a + " is " + result5);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result6 = g * a;
            Console.WriteLine(g + " * " + a + " is " + result6);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result7 = h * a;
            Console.WriteLine(h + " * " + a + " is " + result7);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result8 = i * a;
            Console.WriteLine(i + " * " + a + " is " + result8);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result9 = j * a;
            Console.WriteLine(j + " * " + a + " is " + result9);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result10 = k * a;
            Console.WriteLine(k + " * " + a + " is " + result10);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();
        }
        static public void ruimte()
        {
            double gewichtOpAarde = 69.0;        
            double zwaartekrachtAarde = 1.0;    
            double zwaartekrachtMercurius = 0.38;    
            double zwaartekrachtVenus = 0.91;
            double zwaartekrachtMars = 0.38;
            double zwaartekrachtJupiter = 2.34;
            double zwaartekrachtSaturnus = 1.06;
            double zwaartekrachtUranus = 0.92;
            double zwaartekrachtNeptunus = 1.19;
            double zwaartekrachtPluto = 0.06;

            double gewichtOpMercurius= (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtMercurius; 
            Console.WriteLine("op Mercurius voel je je alsof je " + gewichtOpMercurius + " kg weegt");

            double gewichtOpVenus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtVenus; 
            Console.WriteLine("op Venus voel je je alsof je " + gewichtOpVenus + " kg weegt");

            double gewichtOpMars = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtMars; 
            Console.WriteLine("op Mars voel je je alsof je " + gewichtOpMars + " kg weegt");

            double gewichtOpJupiter = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtJupiter; 
            Console.WriteLine("op Jupiter voel je je alsof je " + Math.Round(gewichtOpJupiter, 2) + " kg weegt");

            double gewichtOpSaturnus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtSaturnus; 
            Console.WriteLine("op Saturnus voel je je alsof je " + gewichtOpSaturnus + " kg weegt");

            double gewichtOpUranus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtUranus;
            Console.WriteLine("op Uranus voel je je alsof je "+ Math.Round(gewichtOpUranus, 2) + " kg weegt");

            double gewichtOpNeptunus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtNeptunus; 
            Console.WriteLine("op Neptunus voel je je alsof je " + gewichtOpNeptunus + " kg weegt");

            double gewichtOpPluto = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtPluto; 
            Console.WriteLine("op Pluto voel je je alsof je " + gewichtOpPluto + " kg weegt");
        }

    }
}
