﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class EenProgrammaSchrijvenInCSharp
    {

        static public void ZegGoedenDag()
        {
            Console.Write("Hoe heet je? ");
            string invoer = Console.ReadLine();
            Console.WriteLine(invoer + ",ik wens je een goededag!");


        }
        static public void MijnEersteProgramma()
        {
            Console.WriteLine("Dit is mijn eerste programma C#-programma");
            Console.WriteLine("........................................");

               

            Console.Write("Typ je voornaam: ");
            string voornaam = Console.ReadLine();
            Console.Write("Typ je achternaam: ");
            string achternaam = Console.ReadLine();

            Console.Write("Dus je naam is: ");
            Console.WriteLine(achternaam + " " + voornaam);

            Console.Write("of: ");
            Console.WriteLine(voornaam + " " + achternaam);
        }
        static public void RommelZin()
        {
            Console.Write("Wat is je favoriete kleur? ");
            string kleur = Console.ReadLine();
            Console.Write("Wat is je favoriete eten? ");
            string eten = Console.ReadLine();
            Console.Write("Wat is je favoriete auto? ");
            string auto = Console.ReadLine();
            Console.Write("Wat is je favoriete film? ");
            string film = Console.ReadLine();
            Console.Write("Wat is je favoriete boek? ");
            string boek = Console.ReadLine();
            Console.WriteLine("Je favoriete kleur is " + kleur+"."+"Je favoriete eten is " + eten+"."+"Je favoriete auto is " + auto+"."+"Je favoriete film is " + film+"."+"Je favoriete boek is " + boek+".");


        }

    }
}
